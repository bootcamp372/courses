import { Component } from '@angular/core';
import { CoursesService } from '../providers/courses.service';
@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.css']
})
export class CoursesListComponent {

  title: string = "Courses Table";
  courses!: Array<any>;
  detailsUrl: string =
  "/course-details/";

  constructor(private coursesService: CoursesService) {
  }

  ngOnInit(): void {
    this.coursesService.getCourses().subscribe(data => {
      this.courses = data;
    });
  }

}
