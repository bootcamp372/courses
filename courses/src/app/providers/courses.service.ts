import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Course } from '../models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  constructor(private http: HttpClient) { }

  private coursesUrl: string =
  'http://localhost:8081/api/courses';

  private httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json'
    })
    };

  getCourses() : Observable<any> {

    return this.http.get(this.coursesUrl,
    this.httpOptions).pipe(map(res => <Course[]>res));
    }

    getCourseDetails(id: number) : Observable<any> {
      return this.http.get(this.coursesUrl + '/' + id,
        this.httpOptions).pipe(map(res => <Course[]>res));
      }
}


